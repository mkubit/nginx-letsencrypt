#!/bin/sh

# generate .conf files from .template files by replacing variables with real envs
for template in `find /etc/nginx/conf.d -name *.template`; do
  echo "substitution of envs inside $template" file
  envsubst "`printf '${%s} ' $(sh -c "env|cut -d'=' -f1")`" < "$template" > "$(echo $template | sed s/\.template/\.conf/g)"
done

# run nginx
nginx -g 'daemon off;'
