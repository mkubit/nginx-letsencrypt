# Nginx Docker image with built-in Letsencrypt support

The easiest way to setup an Nginx based reverse proxy with HTTPS support.

Features:
 * run Letsencrypt certbot (http challange) once a week automatically,
 * run Letsencrypt certbot manually at any time,
 * support for environment variables inside Nginx configuration files.


# Instruction 
## Using Docker cli

**1. Adjust Nginx configuration**

Configuration files:

  * General Nginx server configuration: `nginx.conf`
  * Site/upstream configurations: `conf.d/your-config.template` or `conf.d/your-config.conf`
  
The difference between `.template` and `.conf` files is that the template files will be pre-evaluated with env substitution tool at Nginx startup (and then used as a configs as well).


**2. Build image. You can replace `proxy-ssl` with your own name**

    docker build -t proxy-ssl .

**3. Run Nginx and pass your SSL domain names throug `SSL_DOMAIN_NAME` and `SSL_SUBDOMAINS` (optional)**


    docker run -d \
        --name proxy-ssl \
        -p "80:80" -p "443:443" \
        -v letsencrypt:/etc/letsencrypt \
        -e SSL_DOMAIN_NAME="example.com" \
        -e SSL_SUBDOMAINS="-d sub1.example.com -d sub2.example.com"\
        proxy-ssl
            
Notes:

  * Letsencrypt certificates will be stored inside volume `letsencrypt`

**4. Trigger certbot manually (remember that it also runs automatically every week)**

    docker exec -it proxy-ssl ./renew-https.sh

