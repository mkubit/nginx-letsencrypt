FROM nginx:alpine
#
RUN apk update && apk add certbot

EXPOSE 80 443

# copy dummy cert to allow nginx to start with ssl
RUN mkdir -p /etc/letsencrypt/live/
COPY ssl/dummy-cert/*.pem /etc/letsencrypt/live/

# copy configuration
COPY ssl/snippets/*.conf /etc/nginx/snippets/
COPY nginx.conf /etc/nginx/nginx.conf
COPY conf.d/* /etc/nginx/conf.d/

# ssl renewal scripts
RUN mkdir -p /var/www/letsencrypt
COPY renew-https.sh /
# renew cert every week
RUN ln -s /renew-https.sh /etc/periodic/weekly/renew.sh

# entrypoint
COPY entrypoint.sh /
CMD /entrypoint.sh

