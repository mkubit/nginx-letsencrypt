#!/bin/sh

set -e 

if [ "$SSL_DOMAIN_NAME" == "" ]; then
  echo "Error. You pass SSL_DOMAIN_NAME env to this container!"
  exit 1
fi

certbot certonly --webroot --webroot-path /var/www/letsencrypt/ --renew-by-default --register-unsafely-without-email  --agree-tos -d "$SSL_DOMAIN_NAME" $SSL_SUBDOMAINS
ln -sf "/etc/letsencrypt/live/$SSL_DOMAIN_NAME/fullchain.pem" /etc/letsencrypt/live/fullchain.pem
ln -sf "/etc/letsencrypt/live/$SSL_DOMAIN_NAME/privkey.pem" /etc/letsencrypt/live/privkey.pem

echo "Certificate generation successfull! Reloading nginx config..."
nginx -s reload
echo "Done!"
